"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var FileState;
(function (FileState) {
    FileState[FileState["Synced"] = 0] = "Synced";
    FileState[FileState["Created"] = 1] = "Created";
    FileState[FileState["Updated"] = 2] = "Updated";
    FileState[FileState["Outdated"] = 3] = "Outdated";
    FileState[FileState["RemovedLocal"] = 4] = "RemovedLocal";
    FileState[FileState["RemovedRemote"] = 5] = "RemovedRemote";
})(FileState = exports.FileState || (exports.FileState = {}));
class FileDiff {
    constructor(path) {
        this.localTs = null; // The actual local-FS timestamp of the file
        this.remoteTs = null; // The actual remote-FS timestamp of the file
        this.localSyncTs = null; // The local timestamp of the last sync
        this.localRemoteTs = null; // The remote timestamp of the last sync
        this.path = path;
    }
    getState() {
        if (this.localTs && !this.localRemoteTs && !this.remoteTs) {
            return FileState.Created;
        }
        if (this.remoteTs > this.localRemoteTs) {
            return FileState.Outdated;
        }
        if (this.localTs > this.localSyncTs) {
            return FileState.Updated;
        }
        if (!this.localTs && this.localSyncTs) {
            return FileState.RemovedLocal;
        }
        if (!this.remoteTs && this.localRemoteTs) {
            return FileState.RemovedRemote;
        }
        return FileState.Synced;
    }
}
exports.FileDiff = FileDiff;
class BucketDiff {
    constructor() {
        this.data = {};
    }
    getFile(path) {
        if (this.data[path] === undefined) {
            this.data[path] = new FileDiff(path);
        }
        return this.data[path];
    }
    removeFile(path) {
        delete this.data[path];
    }
    all() {
        return this.data;
    }
}
exports.BucketDiff = BucketDiff;
