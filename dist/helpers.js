"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const util_1 = require("util");
function dump(...objects) {
    console.log(...objects.map((obj) => util_1.inspect(obj, { colors: true, depth: Infinity })));
}
exports.dump = dump;
function each(obj, callable) {
    for (let key in obj) {
        if (obj.hasOwnProperty(key)) {
            callable(obj[key], key);
        }
    }
}
exports.each = each;
