"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const helpers_1 = require("./helpers");
const types_1 = require("./types");
class AvrisSync {
    constructor(local, remote) {
        this.local = local;
        this.remote = remote;
        this.METAFILE = '.avris-sync.json';
    }
    sync() {
        return __awaiter(this, void 0, void 0, function* () {
            const bucketDiff = new types_1.BucketDiff();
            yield this.readMetafile(bucketDiff);
            yield this.local.list('', true).then((files) => {
                helpers_1.each(files, (file) => {
                    if (file.isDir || file.path === this.METAFILE) {
                        return;
                    }
                    bucketDiff.getFile(file.path).localTs = file.timestamp;
                });
            });
            yield this.remote.list('', true).then((files) => {
                helpers_1.each(files, (file) => {
                    if (file.isDir || file.path === this.METAFILE) {
                        return;
                    }
                    bucketDiff.getFile(file.path).remoteTs = file.timestamp;
                });
            });
            const bda = bucketDiff.all();
            for (let key in bda) {
                if (bda.hasOwnProperty(key)) {
                    yield this.syncFile(bucketDiff, bda[key]);
                }
            }
            yield this.writeMetafile(bucketDiff);
            return bucketDiff;
        });
    }
    readMetafile(diff) {
        return __awaiter(this, void 0, void 0, function* () {
            const files = yield this.local.has(this.METAFILE).then((exists) => {
                if (!exists) {
                    return {};
                }
                return this.local.read(this.METAFILE).then((content) => {
                    return JSON.parse(content);
                });
            });
            for (let file in files) {
                if (files.hasOwnProperty(file)) {
                    diff.getFile(file).localSyncTs = files[file][0];
                    diff.getFile(file).localRemoteTs = files[file][1];
                }
            }
            return diff;
        });
    }
    writeMetafile(diff) {
        return __awaiter(this, void 0, void 0, function* () {
            const newMeta = {};
            helpers_1.each(diff.all(), (fileDiff) => {
                newMeta[fileDiff.path] = [
                    fileDiff.localSyncTs,
                    fileDiff.localRemoteTs,
                ];
            });
            yield this.local.put(this.METAFILE, JSON.stringify(newMeta));
            return diff;
        });
    }
    syncFile(bucketDiff, fileDiff) {
        return __awaiter(this, void 0, void 0, function* () {
            switch (fileDiff.getState()) {
                case types_1.FileState.Synced:
                    break;
                case types_1.FileState.Created:
                case types_1.FileState.Updated:
                    yield this.remote.put(fileDiff.path, yield this.local.read(fileDiff.path));
                    fileDiff.localRemoteTs = (yield this.remote.getMetadata(fileDiff.path)).timestamp;
                    fileDiff.localSyncTs = (yield this.local.getMetadata(fileDiff.path)).timestamp;
                    break;
                case types_1.FileState.Outdated:
                    yield this.local.put(fileDiff.path, yield this.remote.read(fileDiff.path));
                    fileDiff.localRemoteTs = (yield this.remote.getMetadata(fileDiff.path)).timestamp;
                    fileDiff.localSyncTs = (yield this.local.getMetadata(fileDiff.path)).timestamp;
                    break;
                case types_1.FileState.RemovedLocal:
                    if (fileDiff.remoteTs) {
                        yield this.remote.remove(fileDiff.path);
                    }
                    bucketDiff.removeFile(fileDiff.path);
                    break;
                case types_1.FileState.RemovedRemote:
                    if (fileDiff.localTs) {
                        yield this.local.remove(fileDiff.path);
                    }
                    bucketDiff.removeFile(fileDiff.path);
                    break;
                default:
                    throw `Invalid state "${fileDiff.getState()}"`;
            }
        });
    }
}
exports.AvrisSync = AvrisSync;
