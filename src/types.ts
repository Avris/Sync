export enum FileState {
    Synced,
    Created,
    Updated,
    Outdated,
    RemovedLocal,
    RemovedRemote,
}

export class FileDiff {
    path: string;
    localTs: number | null = null;       // The actual local-FS timestamp of the file
    remoteTs: number | null = null;      // The actual remote-FS timestamp of the file
    localSyncTs: number | null = null;   // The local timestamp of the last sync
    localRemoteTs: number | null = null; // The remote timestamp of the last sync

    constructor(path) {
        this.path = path;
    }
    
    getState(): FileState {
        if (this.localTs && !this.localRemoteTs && !this.remoteTs) {
            return FileState.Created;
        }

        if (this.remoteTs > this.localRemoteTs) {
            return FileState.Outdated;
        }

        if (this.localTs > this.localSyncTs) {
            return FileState.Updated;
        }

        if (!this.localTs && this.localSyncTs) {
            return FileState.RemovedLocal;
        }

        if (!this.remoteTs && this.localRemoteTs) {
            return FileState.RemovedRemote;
        }

        return FileState.Synced;
    }
}

export class BucketDiff {
    private data: {[name: string]: FileDiff} = {};

    getFile(path: string): FileDiff {
        if (this.data[path] === undefined) {
            this.data[path] = new FileDiff(path);
        }

        return this.data[path];
    }

    removeFile(path: string) {
        delete this.data[path];
    }

    all(): {[name: string]: FileDiff} {
        return this.data;
    }
}

export interface SyncInterface {
    sync(): Promise<BucketDiff>;
}
