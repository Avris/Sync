import {AvrisFilesystem} from "avris-fs/dist/AvrisFilesystem";
import {dump, each} from "./helpers";
import {BucketDiff, FileDiff, FileState, SyncInterface} from "./types";

export class AvrisSync implements SyncInterface {
    private METAFILE: string = '.avris-sync.json';

    constructor (private local: AvrisFilesystem, private remote: AvrisFilesystem) {

    }

    async sync(): Promise<BucketDiff> {
        const bucketDiff = new BucketDiff();

        await this.readMetafile(bucketDiff);

        await this.local.list('', true).then((files) => {
            each(files, (file) => {
                if (file.isDir || file.path === this.METAFILE) {
                    return;
                }

                bucketDiff.getFile(file.path).localTs = file.timestamp;
            });
        });

        await this.remote.list('', true).then((files) => {
            each(files, (file) => {
                if (file.isDir || file.path === this.METAFILE) {
                    return;
                }

                bucketDiff.getFile(file.path).remoteTs = file.timestamp;
            });
        });

        const bda = bucketDiff.all();
        for (let key in bda) {
            if (bda.hasOwnProperty(key)) {
                await this.syncFile(bucketDiff, bda[key]);
            }
        }

        await this.writeMetafile(bucketDiff);

        return bucketDiff;
    }

    private async readMetafile(diff: BucketDiff): Promise<BucketDiff> {
        const files = await this.local.has(this.METAFILE).then((exists) => {
            if (!exists) {
                return {};
            }

            return this.local.read(this.METAFILE).then((content) => {
                return JSON.parse(content);
            })
        });

        for (let file in files) {
            if (files.hasOwnProperty(file)) {
                diff.getFile(file).localSyncTs = files[file][0];
                diff.getFile(file).localRemoteTs = files[file][1];
            }
        }

        return diff;
    }

    private async writeMetafile(diff: BucketDiff): Promise<BucketDiff> {
        const newMeta = {};

        each(diff.all(), (fileDiff) => {
            newMeta[fileDiff.path] = [
                fileDiff.localSyncTs,
                fileDiff.localRemoteTs,
            ]
        });

        await this.local.put(this.METAFILE, JSON.stringify(newMeta));

        return diff;
    }

    private async syncFile(bucketDiff: BucketDiff, fileDiff: FileDiff) {
        switch (fileDiff.getState()) {
            case FileState.Synced:
                break;
            case FileState.Created:
            case FileState.Updated:
                await this.remote.put(
                    fileDiff.path,
                    await this.local.read(fileDiff.path)
                );
                fileDiff.localRemoteTs = (await this.remote.getMetadata(fileDiff.path)).timestamp;
                fileDiff.localSyncTs = (await this.local.getMetadata(fileDiff.path)).timestamp;
                break;
            case FileState.Outdated:
                await this.local.put(
                    fileDiff.path,
                    await this.remote.read(fileDiff.path)
                );

                fileDiff.localRemoteTs = (await this.remote.getMetadata(fileDiff.path)).timestamp;
                fileDiff.localSyncTs = (await this.local.getMetadata(fileDiff.path)).timestamp;
                break;
            case FileState.RemovedLocal:
                if (fileDiff.remoteTs) {
                    await this.remote.remove(fileDiff.path);
                }
                bucketDiff.removeFile(fileDiff.path);
                break;
            case FileState.RemovedRemote:
                if (fileDiff.localTs) {
                    await this.local.remove(fileDiff.path);
                }
                bucketDiff.removeFile(fileDiff.path);
                break;
            default:
                throw `Invalid state "${fileDiff.getState()}"`;
        }
    }
}
