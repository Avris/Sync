import {inspect} from 'util';

export function dump(...objects) {
    console.log(...objects.map((obj) => inspect(obj, { colors: true, depth: Infinity })));
}

export function each(obj, callable) {
    for (let key in obj) {
        if (obj.hasOwnProperty(key)) {
            callable(obj[key], key);
        }
    }
}
