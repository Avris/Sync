# Avris Sync

A simple file synchronisation tool using `avris-fs`.

**Warning!** It's a _very_ early version! Conflict handling is not implemented,
so only use it when you can safely assume often synchronisations/rare conflicts/versioning on the remote, etc. 

## Example

    import {AvrisSync} from "avris-sync";
    import {AvrisFilesystem} from "avris-fs/dist/AvrisFilesystem";
    import {LocalAdapter} from "avris-fs/dist/adapter/LocalAdapter";
    import {S3Adapter} from "avris-fs/dist/adapter/S3Adapter";
    import {dump} from "avris-sync/dist/helpers";
    import {config as dotenv} from 'dotenv';
    
    dotenv();
    
    const as = new AvrisSync(
        new AvrisFilesystem(
            new LocalAdapter(__dirname + '/data/'),
        ),
        new AvrisFilesystem(
            new S3Adapter(
                process.env.AWS_BUCKET,
                process.env.AWS_KEY,
                process.env.AWS_SECRET
            ),
        ),
    );    
    
    as.sync().then((res) => {
        dump(res);
    });

    
## Copyright

* **Author:** Andrzej Prusinowski [(Avris.it)](https://avris.it)
* **Licence:** [MIT](https://mit.avris.it)
